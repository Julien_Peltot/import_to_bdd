#-*- coding:latin-1 -*-

voiture_base = """CREATE TABLE IF NOT EXISTS voiture_base(
    addresse_titulaire TEXT ,
    carrosserie TEXT,
	categorie TEXT ,
    couleur TEXT ,
    cylindree TEXT ,
    date_immatriculation TEXT ,
	denomination_commerciale TEXT ,
    energie TEXT ,
    prenom TEXT ,
	immatriculation TEXT PRIMARY KEY,
    marque TEXT ,
    nom TEXT ,
    places INTEGER ,
	poids INTEGER ,
    puissance INTEGER ,
    type TEXT ,
    variante TEXT ,
	version INTEGER ,
    vin INTEGER 
    );"""

table_voiture = 'voiture_base'

creation_ligne = "INSERT INTO voiture_base VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

update_ligne = """UPDATE voiture_base SET 
    addresse_titulaire=?,
    carrosserie=?,
	categorie=? ,
    couleur=?,
    cylindree=?,
    date_immatriculation=? ,
	denomination_commerciale=? ,
    energie=? ,
    prenom=? ,
    marque=? ,
    nom=? ,
    places=? ,
	poids=? ,
    puissance=? ,
    type=? ,
    variante=? ,
	version=? ,
    vin=? 
    WHERE immatriculation =?; """

selection_immat ="SELECT * FROM voiture_base WHERE immatriculation = (?)"
