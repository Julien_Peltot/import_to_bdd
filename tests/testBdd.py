import unittest
import sqlite3, csv, os
from insert_value import *
from bdd_to_create import voiture_base
class ImportTest(unittest.TestCase):

    def setUp(self):
                
        self.connexion = sqlite3.connect('base_test.sqlite3')
        cursor = self.connexion.cursor()
        cursor.execute(voiture_base)

        self.connection = sqlite3.connect

    def tearDown(self):
        self.connexion.close()
        os.remove('base_test.sqlite3')

    def test_upsert(self):
        self.connexion = sqlite3.connect('base_test.sqlite3')
        cursor = self.connexion.cursor()
        cursor.execute(voiture_base)
        
        nb_ligne =int(cursor.execute("Select count(*) from voiture_base").fetchone()[0])
        
        self.assertEqual(nb_ligne,0)
        with open("test.csv") as csvfile:
            reader = csv.DictReader(csvfile,delimiter=';')
            liste_nom_champ = reader.fieldnames
            nombre_champs = len(reader.fieldnames) 

            if nombre_champs == 19:               
                for ligne in reader: #Pour chaque ligne du fichier csv
                    liste_info = []
                    for entite in liste_nom_champ:
                        liste_info.append(ligne[entite])
                    
                    clef_primaire = cursor.execute(selection_immat,(ligne['immatriculation '],)).fetchone()
                    if not clef_primaire: # (si la ligne n'existe pas)
                        cursor.execute(creation_ligne,tuple(liste_info)) 
        self.assertEqual(nb_ligne,0)
        self.connexion.close()



