#-*- coding:latin-1 -*-
import sqlite3, csv,sys,os,logging
from bdd_to_create import *

def logging_info():
    log_info =logging.basicConfig(filename='auto_loca_web.log',level=logging.DEBUG,\
      format='%(asctime)s -- %(name)s -- %(levelname)s -- %(message)s')
    return log_info

def verif_args():
    if len(sys.argv)<3:
        logging.warning("Arguments insuffisants")
        sys.exit(-1)

def verif_csv(fichier_csv):
    if(os.path.exists(fichier_csv)):
        logging.info(f"CSV {fichier_csv} : existe")
    else: 
        logging.error(f"CSV {fichier_csv} : n'existe pas ")
        sys.exit(-2)


def cree_bdd():
    cursor.execute(voiture_base)

def verif_cree_bdd(fichier_bdd):
    if(os.path.exists(fichier_bdd)):
        logging.info(f"BDD {fichier_bdd}  : existe")
    else: 
        logging.error(f"BDD {fichier_bdd} : n'existe pas ")
        sys.exit(-3)


def verif_cree_table(fichier_bdd):
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    if(cursor.fetchall() != []):
        logging.info(f'Table  {table_voiture} : existe')
    else :
        logging.info(f"Table  {table_voiture} : n'existe pas ")
        commande = open("bdd_to_create.py", 'r')
        cursor.execute(commande.read())
        logging.info(f' La table {table_voiture} est créée')


def import_to_bdd(fichier_csv,fichier_bdd):
    try:
        with open(fichier_csv) as csvfile:
            reader = csv.DictReader(csvfile,delimiter=';')
            liste_nom_champ = reader.fieldnames
            nombre_champs = len(reader.fieldnames) 

            if nombre_champs == 19:               
                for ligne in reader: #Pour chaque ligne du fichier csv
                    liste_info = []
                    for entite in liste_nom_champ:
                        liste_info.append(ligne[entite])
                    
                    clef_primaire = cursor.execute(selection_immat,(ligne['immatriculation '],)).fetchone()
                    if not clef_primaire: # (si la ligne n'existe pas)
                        cursor.execute(creation_ligne,tuple(liste_info)) 
                        connexion.commit()
                        logging.info(f"Ligne {entite} créée car elle n'existe pas")

                    else:
                         cursor.execute(update_ligne,tuple(liste_info))                     
                         connexion.commit()
                         logging.info(f"Ligne {ligne} modifiée car elle existe déjà")
            else:
                 logging.error(f"CSV {fichier_csv} : erreur nombre de champs du fichier")  

    except Exception as e:
        logging.error(f"import dans la bdd {fichier_bdd}: échec %s", e)
   

if __name__ == "__main__":

    fichier_csv = sys.argv[1]
    fichier_bdd = sys.argv[2]

    connexion = sqlite3.connect('base_voiture.sqlite3')
    connexion.text_factory = str
    cursor = connexion.cursor()

    verif_args()
    logging_info()
    verif_csv(fichier_csv)

    cree_bdd()
    verif_cree_bdd(fichier_bdd)

    verif_cree_table(fichier_bdd)
    connexion.commit()

    import_to_bdd(fichier_csv,fichier_bdd)

    connexion.close()